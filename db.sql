CREATE TABLE IF NOT EXISTS `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

INSERT INTO `department` (`id`, `name`) VALUES (1, 'Первый отдел'), (2, 'Второй отдел'), (3, 'Третий отдел'), (4, 'Четвёртый отдел'),(5, 'Пятый отдел');

CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `departmentId` int(11) NOT NULL DEFAULT '0',
  `grade` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_employee_department` (`departmentId`),
  CONSTRAINT `FK_employee_department` FOREIGN KEY (`departmentId`) REFERENCES `department` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

INSERT INTO `employee` (`id`, `name`, `departmentId`, `grade`) VALUES (1, 'Сотрудник 1', 1, 100),(2, 'Сотрудник 2', 1, 200), (3, 'Сотрудник 4', 2, 100), (4, 'Сотрудник 4', 3, 50), (5, 'Сотрудник 5', 5, 100),(6, 'Сотрудник 6', 4, 100), (7, 'Сотрудник 7', 2, 500), (8, 'Сотрудник 8', 3, 100), (9, 'Сотрудник 9', 3, 500), (10, 'Сотрудник 10', 1, 300);

CREATE TABLE IF NOT EXISTS `employee_in_projects` (
  `employeeId` int(11) NOT NULL,
  `projectId` int(11) NOT NULL,
  PRIMARY KEY (`employeeId`,`projectId`),
  KEY `FK_employee_in_projects_project` (`projectId`),
  CONSTRAINT `FK_employee_in_projects_employee` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`id`),
  CONSTRAINT `FK_employee_in_projects_project` FOREIGN KEY (`projectId`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `employee_in_projects` (`employeeId`, `projectId`) VALUES (1, 1), (2, 1), (8, 1), (9, 1), (1, 2), (4, 2), (1, 3), (3, 3), (6, 3), (1, 4), (7, 4), (1, 5), (5, 5);

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

INSERT INTO `project` (`id`, `name`) VALUES (1, 'Первый проект'), (2, 'Второй проект'), (3, 'Третий проект'), (4, 'Четвёртый проект'), (5, 'Пятый проект');