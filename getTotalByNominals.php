<?php

function getTotalByNominals(int $total, array $nominals): array
{
	$output = [];

	if (count($nominals) > 0) {
		foreach ($nominals as $nominalKey => $nominal) {
			if ($total >= $nominal['value'] && $nominal['count'] > 0) {
				$output[$nominal['value']] = min(intdiv($total, $nominal['value']), $nominal['count']);
				$nominalsLeft = array_slice($nominals, $nominalKey + 1);

				while ($output[$nominal['value']] > 0) {
					$totalLeft = $total - ($output[$nominal['value']] * $nominal['value']);
					if ($totalLeft == 0) {
						// Остатка нет - значит уже разложили
						return $output;
					} elseif ($totalLeft > 0) {
						// Ещё есть что разложить
						$potential = getTotalByNominals($totalLeft, $nominalsLeft);
						if (empty($potential)) {
							// Не смогли разложить - значит уменьшаем отложенное кол-во
							$output[$nominal['value']]--;
						} else {
							// Смогли разложить - значит это и есть ответ задачи
							return $output + $potential;
						}
					}
				}

				// Для чистоты вывода
				if ($output[$nominal['value']] == 0) {
					unset($output[$nominal['value']]);
				}
			}
		}
	}

	return $output;
}

$totals = [20, 100, 120, 170, 640, 360, 777, 99990];
$nominals = [
	[
		'value' => 5000,
		'count' => 1000,
	],
	[
		'value' => 1000,
		'count' => 1000,
	],
	[
		'value' => 500,
		'count' => 1000,
	],
	[
		'value' => 100,
		'count' => 1000,
	],
	[
		'value' => 50,
		'count' => 1000,
	],
	[
		'value' => 30,
		'count' => 1000,
	],
];

foreach ($totals as $total) {
	$totalByNominals = getTotalByNominals($total, $nominals);
	if (empty($totalByNominals)) {
		echo "Введена неверная сумма: {$total}\n";
	} else {
		$output = [];
		foreach ($totalByNominals as $nominal => $count) {
			$output[] = "{$count} шт. по {$nominal}";
		}
		echo "Отдаём {$total}: " . implode(', ', $output) . "\n";
	}
}