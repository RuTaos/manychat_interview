SELECT  
prj.id as `prj_id`,
prj.name as `prj_name`,
SUM(ifnull(emp.grade, 0)) as `prj_budget`

FROM `employee_in_projects` tbl
RIGHT JOIN `project` prj ON tbl.projectId = prj.id
LEFT JOIN `employee` emp ON tbl.employeeId = emp.id

GROUP BY prj.id

ORDER BY 3 DESC;

SELECT 
dep.id as `dep_id`,
dep.name as `dep_name`,
emp.id as `emp_id`,
emp.name as `emp_name`,
emp.grade as `emp_grade`

FROM `department` dep
LEFT JOIN `employee` emp ON emp.id = (
	SELECT emp2.id
	FROM `employee` emp2
	WHERE emp2.departmentId = dep.id
	ORDER BY emp2.grade DESC, emp2.id
	LIMIT 1
);